# Support Wheel Of Fate

## 1 App Architecture
### 1.1 General App Architecture
The application uses [Dagger2](https://google.github.io/dagger/) to inject `Managers` (whom maintain side effects and business logic) into different components of the app (fragments, activities, views, services, test cases, presenters, etc...).

**Important Note:** All presenters and classes should interact with the `Managers` **interfaces** and should never interact with API Helper directly.
#### 1.2 Types of Managers:
Managers interfaces can be found under [`data.interfaces`](https://bitbucket.org/zack_barakat/support-wheel-of-fate/src/master/app/src/main/java/com/astro/supportwheeloffate/data/interfaces) package. Each manager is responsible for handling its side of the business logic.
Dagger2 should maintain **only one copy** of each manager per app session (Singlton behaviour).

* **EngineersDataManager**: Responsible for all engineers data such as (fetch engineers, add egineer, and etc...) currently we have fetch engineers function only.
* **ScheduleDataManager**: Responsible for handling schedule businsess logic including applying roles and it provides a schedule for a given set of engineers.
* **AppErrorHelper**: Responsible for hanling all app errors and exceptions.
* **DataManager**: Contains a reference for all other managers for easy injection.

### 1.3 UI componentes architecture
Model View Preseneter known as MVP is the the architecture pattern used to develop Support Wheel Of Fate application UI.
**Model:** It is responsible for handling the data part of the application.
**View:** It is responsible for laying out the views with specific data on the screen.
**Presenter:** It is a bridge that connects a Model and a View. It also acts as an instructor to the View.
To read more about MVP Architecture you may refer to these links:
- [MVP Architecture](https://blog.mindorks.com/essential-guide-for-designing-your-android-app-architecture-mvp-part-1-74efaf1cda40#.lkml1yggq)
- [MVP Android Gudelines](https://medium.com/@cervonefrancesco/model-view-presenter-android-guidelines-94970b430ddf)

## 2 Language used
### 2.1 Java
### 2.2 Kotlin

## 3 Main Libraries used

* [Retrofit](http://square.github.io/retrofit/) - REST client library for android
* [Dagger2](https://google.github.io/dagger/android.html) - Dependency injection framework
* [Anko](https://github.com/Kotlin/anko) - Set of Kotlin extensions to make android development faster
* [RxJava](https://github.com/ReactiveX/RxJava) and [RxAndroid](https://github.com/ReactiveX/RxAndroid) - Reactive programming, simplifies work with threading and concurrency in java and android.
