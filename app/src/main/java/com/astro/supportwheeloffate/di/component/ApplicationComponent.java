package com.astro.supportwheeloffate.di.component;

import android.app.Application;
import android.content.Context;

import com.astro.supportwheeloffate.SupportWheelOfFateApp;
import com.astro.supportwheeloffate.data.interfaces.IAppErrorHandler;
import com.astro.supportwheeloffate.data.interfaces.IDataManager;
import com.astro.supportwheeloffate.data.interfaces.IEngineerDataManager;
import com.astro.supportwheeloffate.data.interfaces.IScheduleDataManager;
import com.astro.supportwheeloffate.di.module.ApiModule;
import com.astro.supportwheeloffate.di.module.ApplicationModule;
import com.astro.supportwheeloffate.di.module.DataManagerModule;
import com.astro.supportwheeloffate.di.qualifiers.ApplicationContext;
import com.astro.supportwheeloffate.di.scopes.ApplicationScope;

import dagger.BindsInstance;
import dagger.Component;

@ApplicationScope
@Component(modules = {
        DataManagerModule.class,
        ApiModule.class,
        ApplicationModule.class})
public interface ApplicationComponent {

    void inject(SupportWheelOfFateApp app);

    @ApplicationContext
    Context context();

    Application application();

    IScheduleDataManager getSchedulerDataManager();

    IEngineerDataManager getEngineerDataManager();

    IDataManager getDataManager();

    IAppErrorHandler getAppErrorHandler();

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        Builder api(ApiModule apiModule);

        ApplicationComponent build();
    }
}