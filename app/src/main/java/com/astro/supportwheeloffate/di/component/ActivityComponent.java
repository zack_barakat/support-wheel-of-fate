package com.astro.supportwheeloffate.di.component;


import com.astro.supportwheeloffate.di.module.ActivityModule;
import com.astro.supportwheeloffate.di.scopes.ActivityScope;
import com.astro.supportwheeloffate.ui.base.BaseMvpActivity;
import com.astro.supportwheeloffate.ui.engineers.EngineersActivity;
import com.astro.supportwheeloffate.ui.schedule.ScheduleActivity;
import com.astro.supportwheeloffate.ui.splash.SplashActivity;

import dagger.Component;


@ActivityScope
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(BaseMvpActivity activity);

    void inject(SplashActivity activity);

    void inject(EngineersActivity activity);

    void inject(ScheduleActivity activity);

}