package com.astro.supportwheeloffate.di.module;

import android.app.Activity;
import android.content.Context;

import com.astro.supportwheeloffate.di.qualifiers.ActivityContext;
import com.astro.supportwheeloffate.ui.engineers.EngineersContracts;
import com.astro.supportwheeloffate.ui.engineers.EngineersPresenter;
import com.astro.supportwheeloffate.ui.schedule.ScheduleContracts;
import com.astro.supportwheeloffate.ui.schedule.SchedulePresenter;
import com.astro.supportwheeloffate.ui.splash.SplashContracts;
import com.astro.supportwheeloffate.ui.splash.SplashPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class ActivityModule {

    private Activity activity;

    public ActivityModule(Activity activity) {
        this.activity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return activity;
    }

    @Provides
    Activity provideActivity() {
        return activity;
    }

    @Provides
    SplashContracts.Presenter<SplashContracts.View> provideSplashPresenter(SplashPresenter splashPresenter) {
        return splashPresenter;
    }

    @Provides
    EngineersContracts.Presenter<EngineersContracts.View> provideEngineersPresenter(EngineersPresenter engineersPresenter) {
        return engineersPresenter;
    }

    @Provides
    ScheduleContracts.Presenter<ScheduleContracts.View> provideSchedulePresenter(SchedulePresenter schedulePresenter) {
        return schedulePresenter;
    }
}
