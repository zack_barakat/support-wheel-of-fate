package com.astro.supportwheeloffate.di.component;


import com.astro.supportwheeloffate.di.module.CustomViewModule;
import com.astro.supportwheeloffate.di.scopes.ViewScope;

import dagger.Component;

@ViewScope
@Component(dependencies = ApplicationComponent.class, modules = CustomViewModule.class)
public interface CustomViewComponent {

}
