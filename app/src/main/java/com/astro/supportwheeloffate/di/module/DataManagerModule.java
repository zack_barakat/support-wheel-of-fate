package com.astro.supportwheeloffate.di.module;


import com.astro.supportwheeloffate.data.AppErrorHandler;
import com.astro.supportwheeloffate.data.DataManager;
import com.astro.supportwheeloffate.data.EngineerDataManager;
import com.astro.supportwheeloffate.data.ScheduleDataManager;
import com.astro.supportwheeloffate.data.interfaces.IAppErrorHandler;
import com.astro.supportwheeloffate.data.interfaces.IDataManager;
import com.astro.supportwheeloffate.data.interfaces.IEngineerDataManager;
import com.astro.supportwheeloffate.data.interfaces.IScheduleDataManager;
import com.astro.supportwheeloffate.di.scopes.ApplicationScope;

import dagger.Module;
import dagger.Provides;


@Module(includes = {ApiModule.class})
public class DataManagerModule {

    @Provides
    @ApplicationScope
    IScheduleDataManager provideISchedulerDataManager(ScheduleDataManager schedulerDataManager) {
        return schedulerDataManager;
    }

    @Provides
    @ApplicationScope
    IEngineerDataManager provideIEngineerDataManager(EngineerDataManager engineerDataManager) {
        return engineerDataManager;
    }

    @Provides
    @ApplicationScope
    public IAppErrorHandler provideIAppErrorHandler(AppErrorHandler handler) {
        return handler;
    }

    @Provides
    @ApplicationScope
    public IDataManager provideDataManager(DataManager manager) {
        return manager;
    }
}
