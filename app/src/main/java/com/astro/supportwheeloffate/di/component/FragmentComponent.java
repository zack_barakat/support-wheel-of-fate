package com.astro.supportwheeloffate.di.component;

import com.astro.supportwheeloffate.di.module.FragmentModule;
import com.astro.supportwheeloffate.di.scopes.FragmentScope;
import com.astro.supportwheeloffate.ui.base.BaseMvpFragment;

import dagger.Component;


@FragmentScope
@Component(dependencies = ApplicationComponent.class, modules = FragmentModule.class)
public interface FragmentComponent {

    void inject(BaseMvpFragment baseMvpFragment);

}