package com.astro.supportwheeloffate.data;

import android.content.Context;

import com.astro.supportwheeloffate.data.interfaces.IEngineerDataManager;
import com.astro.supportwheeloffate.data.model.Engineers;
import com.astro.supportwheeloffate.data.network.IApiHelper;
import com.astro.supportwheeloffate.data.prefs.IPreferencesHelper;
import com.astro.supportwheeloffate.di.qualifiers.ApplicationContext;
import com.astro.supportwheeloffate.di.scopes.ApplicationScope;

import javax.inject.Inject;

import io.reactivex.Observable;


@ApplicationScope
public class EngineerDataManager implements IEngineerDataManager {

    private final Context mContext;
    private final IPreferencesHelper mPreferencesHelper;
    private final IApiHelper mApiHelper;

    @Inject
    public EngineerDataManager(@ApplicationContext Context context,
                               IPreferencesHelper preferencesHelper,
                               IApiHelper apiHelper) {
        mContext = context;
        mPreferencesHelper = preferencesHelper;
        mApiHelper = apiHelper;
    }

    @Override
    public Observable<Engineers> getEngineers() {
        return mApiHelper.getEngineers();
    }

}
