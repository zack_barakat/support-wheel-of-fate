package com.astro.supportwheeloffate.data;

import android.content.Context;

import com.astro.supportwheeloffate.data.interfaces.IAppErrorHandler;
import com.astro.supportwheeloffate.data.interfaces.IDataManager;
import com.astro.supportwheeloffate.data.interfaces.IEngineerDataManager;
import com.astro.supportwheeloffate.data.interfaces.IScheduleDataManager;
import com.astro.supportwheeloffate.di.qualifiers.ApplicationContext;
import com.astro.supportwheeloffate.di.scopes.ApplicationScope;

import javax.inject.Inject;

@ApplicationScope
public class DataManager implements IDataManager {
    private final IAppErrorHandler mAppErrorHandler;
    private final IScheduleDataManager mSchedulerDataManager;
    private final IEngineerDataManager mEngineerDataManager;
    private final Context mApplicationContext;

    @Inject
    public DataManager(IAppErrorHandler appErrorHandler, IEngineerDataManager engineerDataManager, IScheduleDataManager schedulerDataManager, @ApplicationContext Context applicationContext) {
        this.mAppErrorHandler = appErrorHandler;
        this.mEngineerDataManager = engineerDataManager;
        this.mSchedulerDataManager = schedulerDataManager;
        this.mApplicationContext = applicationContext;
    }

    @Override
    public IAppErrorHandler getAppErrorHandler() {
        return mAppErrorHandler;
    }

    @Override
    public IScheduleDataManager getSchedulerDataManager() {
        return mSchedulerDataManager;
    }

    @Override
    public IEngineerDataManager getEngineerDataManager() {
        return mEngineerDataManager;
    }

    @Override
    public Context getApplicationContext() {
        return mApplicationContext;
    }
}
