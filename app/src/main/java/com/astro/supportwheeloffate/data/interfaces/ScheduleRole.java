package com.astro.supportwheeloffate.data.interfaces;

public interface ScheduleRole {
    boolean isValidRole();
}
