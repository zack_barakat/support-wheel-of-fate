package com.astro.supportwheeloffate.data.ScheduleRoles;

import com.astro.supportwheeloffate.data.interfaces.ScheduleRole;
import com.astro.supportwheeloffate.data.model.Day;
import com.astro.supportwheeloffate.data.model.Engineer;

import java.util.ArrayList;

public class OneShiftPerDayRole implements ScheduleRole {
    private final ArrayList<Day> days;
    private final Day currentDay;
    private final Engineer engineer;

    public OneShiftPerDayRole(ArrayList<Day> days, Engineer engineer, Day currentDay) {
        this.days = days;
        this.currentDay = currentDay;
        this.engineer = engineer;
    }

    @Override
    public boolean isValidRole() {

        if (currentDay.getMorningShift() == null) {
            return true;
        }
        return currentDay.getMorningShift() != engineer;
    }
}
