package com.astro.supportwheeloffate.data.interfaces;

import android.content.Context;

import com.astro.supportwheeloffate.di.qualifiers.ApplicationContext;


public interface IDataManager {
    IAppErrorHandler getAppErrorHandler();

    IScheduleDataManager getSchedulerDataManager();

    IEngineerDataManager getEngineerDataManager();

    @ApplicationContext
    Context getApplicationContext();
}
