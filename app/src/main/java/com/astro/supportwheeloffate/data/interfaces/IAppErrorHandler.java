package com.astro.supportwheeloffate.data.interfaces;

import android.support.annotation.NonNull;

import com.astro.supportwheeloffate.data.ErrorAction;

public interface IAppErrorHandler {
    @NonNull
    ErrorAction handleAppException(Throwable exception);

    String getErrorMessage(Throwable throwable);

    String getErrorCode(Throwable throwable);

}
