package com.astro.supportwheeloffate.data.interfaces;


import com.astro.supportwheeloffate.data.model.Engineers;
import com.astro.supportwheeloffate.data.model.Schedule;

import io.reactivex.Observable;


public interface IScheduleDataManager {

    Observable<Schedule> getAllShiftDays(Engineers engineers);
}
