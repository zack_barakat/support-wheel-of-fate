package com.astro.supportwheeloffate.data;


import com.astro.supportwheeloffate.data.ScheduleRoles.MaxTwoShiftPerEngineerRole;
import com.astro.supportwheeloffate.data.ScheduleRoles.OneShiftOnConsecutiveDayRole;
import com.astro.supportwheeloffate.data.ScheduleRoles.OneShiftPerDayRole;
import com.astro.supportwheeloffate.data.interfaces.IScheduleDataManager;
import com.astro.supportwheeloffate.data.interfaces.ScheduleRole;
import com.astro.supportwheeloffate.data.model.Day;
import com.astro.supportwheeloffate.data.model.Engineer;
import com.astro.supportwheeloffate.data.model.Engineers;
import com.astro.supportwheeloffate.data.model.Schedule;
import com.astro.supportwheeloffate.di.scopes.ApplicationScope;
import com.astro.supportwheeloffate.utils.DateUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Random;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;

/**
 * Created by zack_barakat on 19/06/2018.
 */

@ApplicationScope
public class ScheduleDataManager implements IScheduleDataManager {

    private ArrayList<Engineer> mEngineers = new ArrayList<>();

    @Inject
    public ScheduleDataManager() {

    }

    @Override
    public Observable<Schedule> getAllShiftDays(Engineers engineers) {
        return Observable.fromArray(engineers.getEngineers())
                .flatMap((Function<ArrayList<Engineer>, ObservableSource<Schedule>>) engineerList -> {
                    this.mEngineers = engineerList;
                    Collections.shuffle(mEngineers);

                    ArrayList<Day> days = new ArrayList<>(10);
                    ArrayList<Date> dates = DateUtils.getUpcomingWorkingDates(DateUtils.getUpcomingMonday(), 10);

                    for (int i = 0; i < dates.size() - 1; i++) {
                        Day currentDay = new Day();

                        currentDay.setMorningShift(getNextEngineerShit(days, mEngineers, currentDay));
                        currentDay.setEveningShift(getNextEngineerShit(days, mEngineers, currentDay));
                        currentDay.setDate(dates.get(i));

                        days.add(currentDay);
                    }
                    Schedule schedule = new Schedule(days);
                    return Observable.just(schedule);
                });
    }

    private Engineer getNextEngineerShit(ArrayList<Day> days, ArrayList<Engineer> engineers, Day currentDay) {
        while (true) {
            Engineer engineer = pickRandomEngineer(engineers, days);
            ArrayList<ScheduleRole> scheduleRoles = getAllScheduleRoles(days, engineer, currentDay);
            boolean isValidEngineer = true;
            for (ScheduleRole scheduleRole : scheduleRoles) {
                if (!scheduleRole.isValidRole()) {
                    isValidEngineer = false;
                }
            }
            if (isValidEngineer) {
                mEngineers.get(mEngineers.indexOf(engineer)).setNumOfShifts(engineer.getNumOfShifts() + 1);
                return engineer;
            }
        }
    }

    private Engineer pickRandomEngineer(ArrayList<Engineer> engineers, ArrayList<Day> days) {
        ArrayList<Engineer> availableEngineers = new ArrayList<>();

        for (Engineer engineer : engineers) {
            if (engineer.isAvailableForShift() && (engineer.getHasEmptyShifts() || days.size() >= 5)) {
                availableEngineers.add(engineer);
            }
        }
        int randomIndex = new Random().nextInt(availableEngineers.size());
        return availableEngineers.get(randomIndex);
    }

    private ArrayList<ScheduleRole> getAllScheduleRoles(ArrayList<Day> days, Engineer engineer, Day currentDay) {
        ArrayList<ScheduleRole> scheduleRoles = new ArrayList<>();
        scheduleRoles.add(new MaxTwoShiftPerEngineerRole(days, engineer, currentDay));
        scheduleRoles.add(new OneShiftPerDayRole(days, engineer, currentDay));
        scheduleRoles.add(new OneShiftOnConsecutiveDayRole(days, engineer, currentDay));
        return scheduleRoles;
    }
}
