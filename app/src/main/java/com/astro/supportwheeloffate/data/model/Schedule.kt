package com.astro.supportwheeloffate.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

data class Schedule(var days: ArrayList<Day>)

@Parcelize
data class Day(var morningShift: Engineer? = null, var eveningShift: Engineer? = null, var date: Date? = null) : Parcelable
