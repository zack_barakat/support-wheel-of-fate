package com.astro.supportwheeloffate.data.ScheduleRoles;

import com.astro.supportwheeloffate.data.interfaces.ScheduleRole;
import com.astro.supportwheeloffate.data.model.Day;
import com.astro.supportwheeloffate.data.model.Engineer;

import java.util.ArrayList;

public class OneShiftOnConsecutiveDayRole implements ScheduleRole {

    private final ArrayList<Day> days;
    private final Day currentDay;
    private final Engineer engineer;

    public OneShiftOnConsecutiveDayRole(ArrayList<Day> days, Engineer engineer, Day currentDay) {
        this.days = days;
        this.engineer = engineer;
        this.currentDay = currentDay;
    }

    @Override
    public boolean isValidRole() {
        if (days.isEmpty()) {
            return true;
        }
        if (days.get(days.size() - 1).getMorningShift() != engineer &&
                days.get(days.size() - 1).getEveningShift() != engineer) {
            return true;
        }
        return false;
    }
}
