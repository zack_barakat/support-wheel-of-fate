package com.astro.supportwheeloffate.data.network;

import com.astro.supportwheeloffate.data.model.Engineers;

import io.reactivex.Observable;
import retrofit2.Retrofit;
import retrofit2.http.GET;

/**
 * Created by zack_barakat on 19/06/2018.
 */

public interface IApiHelper {


    @GET("/engineers")
    Observable<Engineers> getEngineers();

    class Factory {
        public static final int NETWORK_CALL_TIMEOUT = 30;

        public static IApiHelper create(Retrofit retrofit) {

            return retrofit.create(IApiHelper.class);
        }
    }
}
