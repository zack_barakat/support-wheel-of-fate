package com.astro.supportwheeloffate.data.interfaces;


import com.astro.supportwheeloffate.data.model.Engineers;

import io.reactivex.Observable;


public interface IEngineerDataManager {

    Observable<Engineers> getEngineers();
}
