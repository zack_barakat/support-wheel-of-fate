package com.astro.supportwheeloffate.data;

import android.content.Context;
import android.support.annotation.NonNull;

import com.astro.supportwheeloffate.R;
import com.astro.supportwheeloffate.data.interfaces.IAppErrorHandler;
import com.astro.supportwheeloffate.data.interfaces.IScheduleDataManager;
import com.astro.supportwheeloffate.data.model.CommonResponseModel;
import com.astro.supportwheeloffate.data.network.WheelOfFateException;
import com.astro.supportwheeloffate.di.qualifiers.ApplicationContext;

import javax.inject.Inject;
import javax.net.ssl.HttpsURLConnection;

public class AppErrorHandler implements IAppErrorHandler {
    private final IScheduleDataManager mSchedulerDataManager;
    private final Context appContext;

    @Inject
    public AppErrorHandler(IScheduleDataManager schedulerDataManager, @ApplicationContext Context appContext) {
        this.mSchedulerDataManager = schedulerDataManager;
        this.appContext = appContext;
    }

    @Override
    @NonNull
    public ErrorAction handleAppException(Throwable exception) {
        if (!(exception instanceof WheelOfFateException)) {
            return getUnExpectedErrorAction();
        }
        WheelOfFateException appException = (WheelOfFateException) exception;
        switch (appException.getKind()) {
            case NETWORK:
                return new ErrorAction(ErrorAction.ACTION_TYPE_SHOW_ERROR, appContext.getString(R.string.title_information), appContext.getString(R.string.message_connection_error));
            case HTTP:
                CommonResponseModel apiError = appException.getErrorBodyAsCommonResponseModel();
                if (apiError == null) {
                    return getUnExpectedErrorAction();
                }
                return handleCommonResponseModel(apiError);
            case DISCONNECT:
                return new ErrorAction(ErrorAction.ACTION_TYPE_SHOW_ERROR, appContext.getString(R.string.title_information), appContext.getString(R.string.message_connect_internet));
            case UNEXPECTED:
                return getUnExpectedErrorAction();
        }

        return getUnExpectedErrorAction();
    }

    @Override
    public String getErrorMessage(Throwable throwable) {
        if (!(throwable instanceof WheelOfFateException)) {
            return getUnExpectedErrorAction().getMessageContent();
        }
        WheelOfFateException appException = (WheelOfFateException) throwable;
        switch (appException.getKind()) {
            case NETWORK:
                return appContext.getString(R.string.message_connection_error);
            case HTTP:
                CommonResponseModel apiError = appException.getErrorBodyAsCommonResponseModel();
                if (apiError == null || apiError.getMessage() == null) {
                    return getUnExpectedErrorAction().getMessageContent();
                }
                return apiError.getMessage();
            case DISCONNECT:
                return appContext.getString(R.string.message_connect_internet);
            case UNEXPECTED:
                return getUnExpectedErrorAction().getMessageContent();
        }
        return throwable.getMessage();

    }

    @Override
    public String getErrorCode(Throwable throwable) {
        if (!(throwable instanceof WheelOfFateException)) {
            return null;
        }
        WheelOfFateException appException = (WheelOfFateException) throwable;
        if (appException.getKind().equals(WheelOfFateException.Kind.HTTP)) {
            CommonResponseModel apiError = appException.getErrorBodyAsCommonResponseModel();
            if (apiError == null || apiError.getMessage() == null) {
                return null;
            }
            return apiError.getMessage();
        }
        return null;
    }

    private ErrorAction handleCommonResponseModel(CommonResponseModel apiError) {
        if (apiError.getStatus() == HttpsURLConnection.HTTP_UNAUTHORIZED) {
            // TODO: 20/06/2018 Handle UnAuthorized Request
        }
        if (apiError.getMessage() == null) {
            return getUnExpectedErrorAction();
        }

        return new ErrorAction(ErrorAction.ACTION_TYPE_SHOW_ERROR, appContext.getString(R.string.title_information), apiError.getMessage());
    }

    @NonNull
    private ErrorAction getUnExpectedErrorAction() {
        return new ErrorAction(ErrorAction.ACTION_TYPE_SHOW_ERROR, appContext.getString(R.string.title_information), appContext.getString(R.string.message_error_unknown));
    }
}
