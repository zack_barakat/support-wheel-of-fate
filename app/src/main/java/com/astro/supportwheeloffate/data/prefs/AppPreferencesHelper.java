package com.astro.supportwheeloffate.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.astro.supportwheeloffate.di.qualifiers.ApplicationContext;
import com.astro.supportwheeloffate.di.scopes.ApplicationScope;

import javax.inject.Inject;

@ApplicationScope
public class AppPreferencesHelper implements IPreferencesHelper {

    private static final String PREF_FILE_NAME = "support_wheel_of_fate";
    private final SharedPreferences mSharedPreferences;

    @Inject
    public AppPreferencesHelper(@ApplicationContext Context context) {
        mSharedPreferences = context.getSharedPreferences(PREF_FILE_NAME, Context.MODE_PRIVATE);
    }
}
