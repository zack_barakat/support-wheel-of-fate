package com.astro.supportwheeloffate.data.ScheduleRoles;

import com.astro.supportwheeloffate.data.interfaces.ScheduleRole;
import com.astro.supportwheeloffate.data.model.Day;
import com.astro.supportwheeloffate.data.model.Engineer;

import java.util.ArrayList;

public class MaxTwoShiftPerEngineerRole implements ScheduleRole {

    private final ArrayList<Day> days;
    private final Day currentDay;
    private final Engineer engineer;

    public MaxTwoShiftPerEngineerRole(ArrayList<Day> days, Engineer engineer, Day currentDay) {
        this.days = days;
        this.currentDay = currentDay;
        this.engineer = engineer;
    }

    @Override
    public boolean isValidRole() {
//        int numOfShiftForEngineer = 0;

//        for (int i = 0; i < days.size() - 1; i++) {
//            Day day = days.get(i);
//            if (day.getMorningShift() != null && day.getMorningShift() == engineer) {
//                numOfShiftForEngineer++;
//            }
//            if (day.getMorningShift() != null && day.getMorningShift() == engineer) {
//                numOfShiftForEngineer++;
//            }
////            if (numOfShiftForEngineer >= 2) {
////                engineer.setAvailableForShift(false);
////                return false;
////            }
//        }
        return engineer.getNumOfShifts() < 2;
    }
}
