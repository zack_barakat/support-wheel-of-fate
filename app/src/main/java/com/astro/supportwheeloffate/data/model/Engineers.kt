package com.astro.supportwheeloffate.data.model

import android.os.Parcelable
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Engineers(var engineers: ArrayList<Engineer>) : Parcelable

@Parcelize
data class Engineer @JvmOverloads constructor(var id: Int, var name: String, var numOfShifts: Int = 0) : Parcelable {

    @IgnoredOnParcel
    val isAvailableForShift: Boolean
        get() = numOfShifts < 2

    @IgnoredOnParcel
    val hasEmptyShifts: Boolean
        get() = numOfShifts == 0

    override fun equals(other: Any?): Boolean {
        if (other == null || other !is Engineer) return false
        return id == other.id && name == other.name
    }
}