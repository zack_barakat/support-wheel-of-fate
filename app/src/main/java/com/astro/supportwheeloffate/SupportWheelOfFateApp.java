package com.astro.supportwheeloffate;

import android.app.Application;

import com.astro.supportwheeloffate.di.component.ApplicationComponent;
import com.astro.supportwheeloffate.di.component.DaggerApplicationComponent;
import com.astro.supportwheeloffate.di.module.ApiModule;

public class SupportWheelOfFateApp extends Application {

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        initDagger();
    }

    protected void initDagger() {

        ApplicationComponent appComponent = DaggerApplicationComponent
                .builder()
                .api(new ApiModule(BuildConfig.API))
                .application(this)
                .build();

        setComponent(appComponent);
    }

    public ApplicationComponent getComponent() {
        return component;
    }

    public void setComponent(ApplicationComponent component) {
        this.component = component;
        component.inject(this);
    }

}
