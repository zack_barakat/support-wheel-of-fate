package com.astro.supportwheeloffate.ui.engineers

import com.astro.supportwheeloffate.R
import com.astro.supportwheeloffate.data.interfaces.IDataManager
import com.astro.supportwheeloffate.data.model.Engineers
import com.astro.supportwheeloffate.ui.base.BaseMvpPresenter
import com.astro.supportwheeloffate.ui.base.ErrorView
import javax.inject.Inject

class EngineersPresenter @Inject
constructor(dataManager: IDataManager) : BaseMvpPresenter<EngineersContracts.View>(dataManager), EngineersContracts.Presenter<EngineersContracts.View> {

    private var mEngineers: Engineers? = null

    override fun onAttachView(v: EngineersContracts.View?) {
        super.onAttachView(v)
        fetchEngineers()
    }

    override fun onRefreshEngineersClick() {
        fetchEngineers()
    }

    override fun onGenerateScheduleClick() {
        if (mEngineers == null) {
            view.showError(R.string.message_error_no_engineers, ErrorView.ERROR_TOAST)
            return
        }
        view.openScheduleScreen(mEngineers!!)
    }

    private fun fetchEngineers() {
        view.showProgress()
        val disposable = mEngineerDataManager.engineers
                .subscribe({
                    mEngineers = it
                    view.hideProgress()
                    view.showEngineers(it.engineers)
                }, {
                    handleApiError(it, ErrorView.ERROR_DIALOG)
                    view.hideProgress()
                })
        addToSubscription(disposable)
    }
}
