package com.astro.supportwheeloffate.ui.schedule

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.astro.supportwheeloffate.R
import com.astro.supportwheeloffate.data.model.Day
import kotlinx.android.synthetic.main.item_shift.view.*
import java.text.SimpleDateFormat
import java.util.*

class ScheduleAdapter : RecyclerView.Adapter<ScheduleAdapter.ScheduleViewHolder>() {

    private var days = arrayListOf<Day>()

    private var mIsListManager: Boolean = true

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScheduleViewHolder {
        if (mIsListManager) {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_shift, parent, false)
            return ScheduleViewHolder(view, mIsListManager)
        } else {
            val view = LayoutInflater.from(parent.context).inflate(R.layout.item_shift_grid, parent, false)
            return ScheduleViewHolder(view, mIsListManager)
        }
    }

    override fun getItemCount() = days.size

    override fun onBindViewHolder(holder: ScheduleViewHolder, position: Int) = holder.bind(days[position])

    fun refreshSchedule(days: ArrayList<Day>) {
        this.days = days
        notifyDataSetChanged()
    }

    fun updateLayoutManager(isListManager: Boolean) {
        this.mIsListManager = isListManager
    }

    class ScheduleViewHolder(itemView: View, val mIsListManager: Boolean) : RecyclerView.ViewHolder(itemView) {
        fun bind(day: Day) = with(itemView) {
            with(day) {
                tvDate.text = getDisplayDate(date)
                tvDay.text = getDisplayDay(date)
                tvDayShiftEngineerName.text = morningShift?.name
                tvNightShiftEngineerName.text = eveningShift?.name
            }
        }

        private fun getDisplayDay(date: Date?): CharSequence? {
            val pattern = if (mIsListManager) "EEEE" else "EEE"
            val simpleDateFormat = SimpleDateFormat(pattern, Locale.getDefault())
            return simpleDateFormat.format(date);
        }

        private fun getDisplayDate(date: Date?): String? {
            val pattern = if (mIsListManager) "dd/MM/yyyy" else "dd/MM"
            val simpleDateFormat = SimpleDateFormat(pattern, Locale.getDefault())
            return simpleDateFormat.format(date);
        }
    }
}