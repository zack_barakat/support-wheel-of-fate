package com.astro.supportwheeloffate.ui.engineers

import com.astro.supportwheeloffate.data.model.Engineer
import com.astro.supportwheeloffate.data.model.Engineers
import com.astro.supportwheeloffate.ui.base.BasePresenter
import com.astro.supportwheeloffate.ui.base.BaseView

interface EngineersContracts {

    interface View : BaseView {
        fun showEngineers(engineers: ArrayList<Engineer>)

        fun showEmptyEngineers()

        fun openScheduleScreen(engineers: Engineers)
    }

    interface Presenter<V : View> : BasePresenter<V> {
        fun onRefreshEngineersClick()

        fun onGenerateScheduleClick()
    }
}
