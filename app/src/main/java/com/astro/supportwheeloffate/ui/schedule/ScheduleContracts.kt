package com.astro.supportwheeloffate.ui.schedule

import com.astro.supportwheeloffate.data.model.Day
import com.astro.supportwheeloffate.data.model.Engineers
import com.astro.supportwheeloffate.ui.base.BasePresenter
import com.astro.supportwheeloffate.ui.base.BaseView

interface ScheduleContracts {

    interface View : BaseView {
        fun showShifts(days: ArrayList<Day>)

        fun changeLayoutManager(toListManager: Boolean)
    }

    interface Presenter<V : View> : BasePresenter<V> {
        fun initWithEngineers(engineers: Engineers)

        fun onChangeLayoutManagerClick()
    }
}
