package com.astro.supportwheeloffate.ui.base;

public interface BaseView extends LoadingView, ErrorView {

    void destroyView();
}
