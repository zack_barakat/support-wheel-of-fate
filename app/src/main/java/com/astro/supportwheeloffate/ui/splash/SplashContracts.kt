package com.astro.supportwheeloffate.ui.splash

import com.astro.supportwheeloffate.ui.base.BasePresenter
import com.astro.supportwheeloffate.ui.base.BaseView

interface SplashContracts {

    interface View : BaseView {

        fun showMainScreen()
    }

    interface Presenter<V : View> : BasePresenter<V>
}
