package com.astro.supportwheeloffate.ui.schedule

import com.astro.supportwheeloffate.data.interfaces.IDataManager
import com.astro.supportwheeloffate.data.model.Engineers
import com.astro.supportwheeloffate.ui.base.BaseMvpPresenter
import com.astro.supportwheeloffate.ui.base.ErrorView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SchedulePresenter @Inject
constructor(dataManager: IDataManager) : BaseMvpPresenter<ScheduleContracts.View>(dataManager), ScheduleContracts.Presenter<ScheduleContracts.View> {

    private var mEngineers: Engineers? = null
    private var mIsListManager: Boolean = true

    override fun onResume() {
        if (mEngineers == null) {
            return
        }
        view.showProgress()
        val disposable = mSchedulerDataManager.getAllShiftDays(mEngineers!!)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.hideProgress()
                    view.showShifts(it.days)
                }, {
                    handleApiError(it, ErrorView.ERROR_DIALOG)
                    view.hideProgress()
                })
        addToSubscription(disposable)
    }

    override fun onChangeLayoutManagerClick() {
        if (mIsListManager) {
            mIsListManager = false
            view.changeLayoutManager(false)
        } else {
            mIsListManager = true
            view.changeLayoutManager(true)
        }
    }

    override fun initWithEngineers(engineers: Engineers) {
        mEngineers = engineers
    }
}
