package com.astro.supportwheeloffate.ui.splash

import android.os.Bundle
import com.astro.supportwheeloffate.R
import com.astro.supportwheeloffate.ui.base.BaseMvpActivity
import com.astro.supportwheeloffate.ui.base.BasePresenter
import com.astro.supportwheeloffate.ui.engineers.EngineersActivity
import org.jetbrains.anko.clearTask
import org.jetbrains.anko.intentFor
import org.jetbrains.anko.newTask
import javax.inject.Inject

class SplashActivity : BaseMvpActivity(), SplashContracts.View {

    @Inject
    lateinit var mPresenter: SplashContracts.Presenter<SplashContracts.View>

    public override fun onCreate(savedInstanceState: Bundle?) {
        activityComponent?.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setupLayout()
        mPresenter.onAttachView(this)
    }

    override fun sendExtrasToPresenter(extras: Bundle) {

    }

    protected fun setupLayout() {

    }

    public override fun getPresenter(): BasePresenter<*> {
        return mPresenter
    }

    override fun showMainScreen() {
        startActivity(intentFor<EngineersActivity>().clearTask().newTask())
    }
}
