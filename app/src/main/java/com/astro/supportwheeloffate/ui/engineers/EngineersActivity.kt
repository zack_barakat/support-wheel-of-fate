package com.astro.supportwheeloffate.ui.engineers

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.astro.supportwheeloffate.R
import com.astro.supportwheeloffate.data.model.Engineer
import com.astro.supportwheeloffate.data.model.Engineers
import com.astro.supportwheeloffate.ui.base.BaseMvpActivity
import com.astro.supportwheeloffate.ui.base.BasePresenter
import com.astro.supportwheeloffate.ui.schedule.ScheduleActivity
import kotlinx.android.synthetic.main.activity_engineers.*
import org.jetbrains.anko.startActivity
import javax.inject.Inject

class EngineersActivity : BaseMvpActivity(), EngineersContracts.View {

    @Inject
    lateinit var mPresenter: EngineersContracts.Presenter<EngineersContracts.View>

    private var mAdapter = EngineersAdapter()

    public override fun onCreate(savedInstanceState: Bundle?) {
        activityComponent?.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_engineers)
        setupLayout()
        mPresenter.onAttachView(this)
    }

    override fun sendExtrasToPresenter(extras: Bundle) {

    }

    private fun setupLayout() {
        supportActionBar?.title = getString(R.string.title_engineers)
        rvEngineers.layoutManager = LinearLayoutManager(this)
        rvEngineers.adapter = mAdapter
        swipeRefresh.setOnRefreshListener { mPresenter.onRefreshEngineersClick() }
        btnGenerateSchedule.setOnClickListener { mPresenter.onGenerateScheduleClick() }
    }

    public override fun getPresenter(): BasePresenter<*> = mPresenter

    override fun showEngineers(engineers: ArrayList<Engineer>) {
        mAdapter.refreshEngineers(engineers)
    }

    override fun showEmptyEngineers() {

    }

    override fun openScheduleScreen(engineers: Engineers) {
        startActivity<ScheduleActivity>(ScheduleActivity.EXTRA_ENGINEERS to engineers)
    }

    override fun showProgress() {
        swipeRefresh.isRefreshing = true
    }

    override fun hideProgress() {
        swipeRefresh.isRefreshing = false
    }
}
