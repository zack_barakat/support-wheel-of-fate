package com.astro.supportwheeloffate.ui.engineers

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.astro.supportwheeloffate.R
import com.astro.supportwheeloffate.data.model.Engineer
import kotlinx.android.synthetic.main.item_engineer.view.*

class EngineersAdapter : RecyclerView.Adapter<EngineersAdapter.EngineerViewHolder>() {

    private var engineers = arrayListOf<Engineer>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EngineerViewHolder {
        return EngineerViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_engineer, parent, false)
        )
    }

    override fun getItemCount() = engineers.size

    override fun onBindViewHolder(holder: EngineerViewHolder, position: Int) = holder.bind(engineers[position])

    fun refreshEngineers(data: ArrayList<Engineer>) {
        this.engineers = data
        notifyDataSetChanged()
    }

    class EngineerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(engineer: Engineer) = with(itemView) {
            with(engineer) {
                tvEngineerName.text = name
            }
        }
    }
}