package com.astro.supportwheeloffate.ui.base;

public interface LoadingView {
    void showProgress();

    void hideProgress();
}
