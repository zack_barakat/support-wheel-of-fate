package com.astro.supportwheeloffate.ui.schedule

import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.util.DisplayMetrics
import android.view.ViewTreeObserver
import com.astro.supportwheeloffate.R
import com.astro.supportwheeloffate.data.model.Day
import com.astro.supportwheeloffate.data.model.Engineers
import com.astro.supportwheeloffate.ui.base.BaseMvpActivity
import com.astro.supportwheeloffate.ui.base.BasePresenter
import kotlinx.android.synthetic.main.activity_schedule.*
import javax.inject.Inject


class ScheduleActivity : BaseMvpActivity(), ScheduleContracts.View {


    companion object {
        const val EXTRA_ENGINEERS = "extra_engineers"
    }

    @Inject
    lateinit var mPresenter: ScheduleContracts.Presenter<ScheduleContracts.View>

    private var mAdapter = ScheduleAdapter()

    private var mSpanCount: Int = 3
        set(value) {
            if (value < 2) {
                field = 2
            } else {
                field = value
            }
        }

    public override fun onCreate(savedInstanceState: Bundle?) {
        activityComponent?.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule)
        setupLayout()
        mPresenter.onAttachView(this)
    }

    override fun sendExtrasToPresenter(extras: Bundle) {
        val engineers: Engineers = intent.extras.getParcelable(EXTRA_ENGINEERS)
        mPresenter.initWithEngineers(engineers)
    }

    private fun setupLayout() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.title_schedule)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        ivChangeRvManager.setOnClickListener { mPresenter.onChangeLayoutManagerClick() }
        calculateColumnCount()
        rvSchedule.layoutManager = LinearLayoutManager(this)
        rvSchedule.adapter = mAdapter
    }

    private fun calculateColumnCount() {
        val vto = rvSchedule.viewTreeObserver
        vto.addOnPreDrawListener(object : ViewTreeObserver.OnPreDrawListener {
            override fun onPreDraw(): Boolean {
                mSpanCount = Math.floor((rvSchedule.width / convertDPToPixels(90)).toDouble()).toInt()
                when {
                    vto.isAlive -> vto.removeOnPreDrawListener(this)
                    else -> rvSchedule.viewTreeObserver.removeOnPreDrawListener(this)
                }
                return true
            }
        })
    }

    private fun convertDPToPixels(dp: Int): Float {
        val metrics = DisplayMetrics()

        windowManager.defaultDisplay.getMetrics(metrics)
        return dp * metrics.density
    }

    public override fun getPresenter(): BasePresenter<*> = mPresenter

    override fun showShifts(days: ArrayList<Day>) {
        mAdapter.refreshSchedule(days)
    }

    override fun changeLayoutManager(toListManager: Boolean) {
        if (toListManager) {
            rvSchedule.layoutManager = LinearLayoutManager(this)
            mAdapter.updateLayoutManager(toListManager)
            rvSchedule.adapter = mAdapter
            ivChangeRvManager.setImageResource(R.drawable.ic_grid_white_24dp)
        } else {
            rvSchedule.layoutManager = GridLayoutManager(this, mSpanCount)
            mAdapter.updateLayoutManager(toListManager)
            rvSchedule.adapter = mAdapter
            ivChangeRvManager.setImageResource(R.drawable.ic_list_white_24dp)
        }
    }
}
