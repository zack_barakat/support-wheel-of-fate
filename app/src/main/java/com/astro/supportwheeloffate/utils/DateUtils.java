package com.astro.supportwheeloffate.utils;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static java.util.Calendar.DAY_OF_WEEK;

public class DateUtils {
    @NonNull
    public static Date getUpcomingMonday() {
        Calendar c = new GregorianCalendar();
        c.setTime(new Date());
        while (true) {
            if (Calendar.MONDAY == c.get(DAY_OF_WEEK)) {
                return c.getTime();
            }
            c.add(Calendar.DAY_OF_YEAR, 1);
        }
    }

    public static ArrayList<Date> getUpcomingWorkingDates(Date startDate, int span) {
        ArrayList<Date> dates = new ArrayList<>();

        Calendar c = new GregorianCalendar();
        c.setTime(startDate);
        while (dates.size() < span) {
            if (Calendar.SATURDAY != c.get(DAY_OF_WEEK) && Calendar.SUNDAY != c.get(DAY_OF_WEEK)) {
                dates.add(c.getTime());
            }
            c.add(Calendar.DAY_OF_YEAR, 1);
        }
        return dates;
    }
}
