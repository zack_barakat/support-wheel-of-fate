package com.astro.supportwheeloffate.di.module;


import com.astro.supportwheeloffate.data.TestApiHelper;
import com.astro.supportwheeloffate.data.network.IApiHelper;
import com.astro.supportwheeloffate.di.scopes.ApplicationScope;

import dagger.Module;
import dagger.Provides;

import static org.mockito.Mockito.spy;


@Module(includes = {TestApplicationModule.class})
public class MockApiModule {

    @Provides
    @ApplicationScope
    IApiHelper provideApiHelper() {
        return spy(new TestApiHelper());
    }

}

