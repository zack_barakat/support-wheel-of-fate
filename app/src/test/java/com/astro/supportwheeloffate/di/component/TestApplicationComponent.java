package com.astro.supportwheeloffate.di.component;


import android.app.Application;
import android.content.Context;

import com.astro.supportwheeloffate.SupportWheelOfFateApp;
import com.astro.supportwheeloffate.data.interfaces.IAppErrorHandler;
import com.astro.supportwheeloffate.data.interfaces.IDataManager;
import com.astro.supportwheeloffate.data.interfaces.IEngineerDataManager;
import com.astro.supportwheeloffate.data.interfaces.IScheduleDataManager;
import com.astro.supportwheeloffate.data.network.IApiHelper;
import com.astro.supportwheeloffate.data.prefs.IPreferencesHelper;
import com.astro.supportwheeloffate.di.module.MockApiModule;
import com.astro.supportwheeloffate.di.module.TestApplicationModule;
import com.astro.supportwheeloffate.di.module.TestDataManagerModule;
import com.astro.supportwheeloffate.di.qualifiers.ApplicationContext;
import com.astro.supportwheeloffate.di.scopes.ApplicationScope;
import com.astro.supportwheeloffate.mvp.presenter.EngineersPresenterTest;
import com.astro.supportwheeloffate.testCase.WheelOfFateRobolectricTestCase;

import dagger.BindsInstance;
import dagger.Component;

@ApplicationScope
@Component(modules = {
        MockApiModule.class,
        TestDataManagerModule.class,
        TestApplicationModule.class})
public interface TestApplicationComponent extends ApplicationComponent {

    void inject(SupportWheelOfFateApp app);

    @ApplicationContext
    Context context();

    void inject(WheelOfFateRobolectricTestCase wheelOfFateRobolectricTestCase);


    void inject(EngineersPresenterTest historyPresenterTest);

//    void inject(InboxPresenterTest inboxPresenterTest);

    @Component.Builder
    interface Builder {
        @BindsInstance
        TestApplicationComponent.Builder application(Application application);

        TestApplicationComponent build();
    }

    Application application();

    IScheduleDataManager getScheduleDataManager();

    IEngineerDataManager getEngineerDataManager();

    IPreferencesHelper getPreferenceHelper();

    IApiHelper getApiHelper();

    IAppErrorHandler getAppErrorHandler();

    IDataManager getDataManager();

}
