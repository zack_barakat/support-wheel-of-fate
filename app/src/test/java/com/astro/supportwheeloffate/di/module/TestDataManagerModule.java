package com.astro.supportwheeloffate.di.module;

import com.astro.supportwheeloffate.data.AppErrorHandler;
import com.astro.supportwheeloffate.data.DataManager;
import com.astro.supportwheeloffate.data.EngineerDataManager;
import com.astro.supportwheeloffate.data.ScheduleDataManager;
import com.astro.supportwheeloffate.data.interfaces.IAppErrorHandler;
import com.astro.supportwheeloffate.data.interfaces.IDataManager;
import com.astro.supportwheeloffate.data.interfaces.IEngineerDataManager;
import com.astro.supportwheeloffate.data.interfaces.IScheduleDataManager;
import com.astro.supportwheeloffate.di.scopes.ApplicationScope;

import dagger.Module;
import dagger.Provides;

import static org.mockito.Mockito.spy;


@Module(includes = {MockApiModule.class})
public class TestDataManagerModule {

    @Provides
    @ApplicationScope
    IScheduleDataManager provideIScheduleDataManager(ScheduleDataManager scheduleDataManager) {
        return spy(scheduleDataManager);
    }

    @Provides
    @ApplicationScope
    IEngineerDataManager provideIEngineerDataManager(EngineerDataManager engineerDataManager) {
        return spy(engineerDataManager);
    }

    @Provides
    @ApplicationScope
    public IDataManager provideDataManager(DataManager manager) {
        return spy(manager);
    }

    @Provides
    @ApplicationScope
    public IAppErrorHandler provideAppErrorHandler(AppErrorHandler handler) {
        return spy(handler);
    }
}
