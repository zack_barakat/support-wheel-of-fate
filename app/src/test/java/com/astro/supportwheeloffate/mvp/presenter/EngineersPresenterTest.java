package com.astro.supportwheeloffate.mvp.presenter;


import com.astro.supportwheeloffate.rule.TestSchedulersRule;
import com.astro.supportwheeloffate.testCase.WheelOfFateRobolectricTestCase;
import com.astro.supportwheeloffate.ui.engineers.EngineersContracts;
import com.astro.supportwheeloffate.ui.engineers.EngineersPresenter;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.mockito.Mock;

import javax.inject.Inject;

import static org.mockito.Mockito.spy;


public class EngineersPresenterTest extends WheelOfFateRobolectricTestCase {

    @Inject
    EngineersPresenter mPresenter;

    @Mock
    EngineersContracts.View mView;

    @Rule
    public TestSchedulersRule rule = new TestSchedulersRule();

    @Before
    @Override
    public void setUp() throws Exception {
        component().inject(this);
        super.setUp();
        mPresenter = spy(mPresenter);
    }

    @After
    public void unsubAll() {
        mPresenter.onDestroyView();
    }

}
