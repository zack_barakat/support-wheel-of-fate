package com.astro.supportwheeloffate.testUtils;

import com.astro.supportwheeloffate.data.model.Engineers;
import com.google.gson.Gson;

public class TestDataGenerator {
    private static Gson mGson = new Gson();

    public static Engineers getEngineers_Positive() {
        return mGson.fromJson(" {\n" +
                "   \"status\": 200,\n" +
                "   \"message\": [],\n" +
                "   \"hail_code\": \"1234\",\n" +
                "   \"ttl\": 14\n" +
                " }", Engineers.class);
    }
}
