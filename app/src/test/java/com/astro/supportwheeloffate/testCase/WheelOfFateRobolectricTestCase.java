package com.astro.supportwheeloffate.testCase;

import android.content.Context;
import android.support.annotation.NonNull;

import com.astro.supportwheeloffate.MockSupportWheelOfFateApp;
import com.astro.supportwheeloffate.data.interfaces.IEngineerDataManager;
import com.astro.supportwheeloffate.data.interfaces.IScheduleDataManager;
import com.astro.supportwheeloffate.data.network.IApiHelper;
import com.astro.supportwheeloffate.data.prefs.IPreferencesHelper;
import com.astro.supportwheeloffate.di.component.TestApplicationComponent;
import com.astro.supportwheeloffate.shadows.ShadowPreferenceManager;
import com.astro.supportwheeloffate.testRunner.WheelOfFateRobolectricTestRunner;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.multidex.ShadowMultiDex;

import javax.inject.Inject;


@RunWith(WheelOfFateRobolectricTestRunner.class)
@Config(
        application = MockSupportWheelOfFateApp.class,
        shadows = {ShadowMultiDex.class, ShadowPreferenceManager.class},
        sdk = WheelOfFateRobolectricTestRunner.DEFAULT_SDK,
        manifest = "src/main/AndroidManifest.xml"
)
public abstract class WheelOfFateRobolectricTestCase extends TestCase {

    private MockSupportWheelOfFateApp application;


    @Inject
    public IEngineerDataManager engineerDataManager;

    @Inject
    public IScheduleDataManager scheduleDataManager;

    @Inject
    public IPreferencesHelper preferenceHelper;

    @Inject
    public IApiHelper apiHelper;

    @Override
    @Before
    public void setUp() throws Exception {
        component().inject(this);
        MockitoAnnotations.initMocks(this);
        super.setUp();
    }

    protected
    @NonNull
    MockSupportWheelOfFateApp application() {
        if (application != null) {
            return application;
        }

        application = (MockSupportWheelOfFateApp) RuntimeEnvironment.application;
        return application;
    }

    protected
    @NonNull
    TestApplicationComponent component() {
        if (application != null) {
            return (TestApplicationComponent) application.getComponent();
        }

        application = (MockSupportWheelOfFateApp) RuntimeEnvironment.application;
        return (TestApplicationComponent) application.getComponent();
    }

    protected
    @NonNull
    Context context() {
        return application().getApplicationContext();
    }

}
