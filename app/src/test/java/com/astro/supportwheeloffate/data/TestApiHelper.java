package com.astro.supportwheeloffate.data;

import com.astro.supportwheeloffate.data.model.Engineers;
import com.astro.supportwheeloffate.data.network.IApiHelper;
import com.astro.supportwheeloffate.di.scopes.ApplicationScope;

import io.reactivex.Observable;

import static org.mockito.Mockito.mock;


//TODO: replace with response data
@ApplicationScope
public class TestApiHelper implements IApiHelper {

    @Override
    public Observable<Engineers> getEngineers() {
        Engineers model = mock(Engineers.class);
        return Observable.just(model);
    }

}
