package com.astro.supportwheeloffate;

import android.content.SharedPreferences;

import com.astro.supportwheeloffate.di.component.TestApplicationComponent;
import com.astro.supportwheeloffate.shadows.ShadowPreferenceManager;

public class MockSupportWheelOfFateApp extends SupportWheelOfFateApp {

    @Override
    public void onCreate() {
        super.onCreate();
    }


    @Override
    public void initDagger() {

        TestApplicationComponent applicationComponent = DaggerTestApplicationComponent.builder()
                .application(this)
                .build();
        setComponent(applicationComponent);
    }


    @Override
    public SharedPreferences getSharedPreferences(String name, int mode) {
        return ShadowPreferenceManager.getDefaultSharedPreferences(this);
    }
}
